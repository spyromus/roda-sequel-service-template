# frozen_string_literal: true

class ItemCreatedEvent < BaseEvent
  attribute :id, Types::Integer
  attribute :name, Types::String
  attribute :price, Types::Coercible::Integer

  def self.from_item(item)
    new(
      id: item.id,
      name: item.name,
      price: item.price
    )
  end
end
