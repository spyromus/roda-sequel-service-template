# frozen_string_literal: true

require 'dry-struct'

class DeleteItemCommand < BaseCommand
  attribute :id, Types::Integer
end
