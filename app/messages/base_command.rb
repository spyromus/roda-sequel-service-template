# frozen_string_literal: true

# BaseCommand is the base class for all commands with meaningful defaults.
class BaseCommand < Dry::Struct
  def command?
    true
  end
end
