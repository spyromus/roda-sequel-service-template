# frozen_string_literal: true

require 'dry-struct'

class CreateItemCommand < BaseCommand
  attribute :name, Types::String
  attribute :price, Types::Coercible::Integer
end
