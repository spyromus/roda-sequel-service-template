# frozen_string_literal: true

require 'dry-struct'

class UpdateItemCommand < BaseCommand
  attribute :id, Types::Integer
  attribute :name, Types::String
  attribute :price, Types::Coercible::Integer
end
