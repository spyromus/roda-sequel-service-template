# frozen_string_literal: true

class ItemDeletedEvent < BaseEvent
  attribute :id, Types::Integer
end
