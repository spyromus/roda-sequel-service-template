# frozen_string_literal: true

# BaseEvent is the base class for all events with meaningful defaults.
class BaseEvent < Dry::Struct
  def event?
    true
  end

  def event_name
    self.class.name.gsub('Event', '')
  end

  def event_version
    1
  end

  def payload
    to_h
  end
end
