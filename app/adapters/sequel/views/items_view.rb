# frozen_string_literal: true

module Adapters
  module Sequel
    module Views
      # ItemsView provides the list of items
      class ItemsView
        def initialize(db:)
          @db = db
        end

        # Returns the array of hashes
        def all
          @db[:items].all
        end
      end
    end
  end
end
