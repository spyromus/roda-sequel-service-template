# frozen_string_literal: true

module Adapters
  module Sequel
    module Views
      # ItemView returns complete item info.
      class ItemView
        def initialize(db:)
          @db = db
        end

        def call(id)
          # Returns hash here, not the Item record.
          @db[:items].first(id: id)
        end
      end
    end
  end
end
