# frozen_string_literal: true

require 'logger'
require 'sequel'

module Adapters
  module Sequel
    # DatabaseFactory provides connections to database via Sequel
    class DatabaseFactory
      def initialize(database_url)
        @database_url = database_url
      end

      def create_connection
        ::Sequel.connect(@database_url, logger: Logger.new($stdout))
      end
    end
  end
end
