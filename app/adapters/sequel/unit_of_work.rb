# frozen_string_literal: true

module Adapters
  module Sequel
    # UnitOfWork provides the implementation of UOW abstraction.
    #
    # Usage example:
    #
    #   result = container[:uow].call do |trx|
    #     rec = trx.my_repo.find(...)
    #
    #     # Transactions are rolled back unless explicitly committed.
    #     trx.commit
    #
    #     rec
    #   end
    class UnitOfWork
      def initialize(db:)
        @db = db
      end

      # Transaction encapsulates the transaction scope
      class Transaction
        attr_reader :committed

        def initialize(db)
          @db = db
          @committed = false
        end

        def items
          @items ||= Repo::ItemsRepository.new(db: @db)
        end

        def commit
          @committed = true
        end
      end

      def call
        trn = Transaction.new(@db)
        @db.transaction do
          res = yield trn

          # Rolling back here requires explicit commit
          raise ::Sequel::Rollback unless trn.committed

          res
        end
      end
    end
  end
end
