# frozen_string_literal: true

module Adapters
  module Sequel
    module Repo
      # ItemsRepository provides access to items in the storage.
      class ItemsRepository
        def initialize(db:)
          @db = db
        end

        # Finds one item or returns nil
        def find(id)
          row = items.first(id: id)
          return unless row

          Item.new(
            id: row[:id],
            name: row[:name],
            price: row[:price]
          )
        end

        # Adds item to repository.
        # Returns a NEW item object with ID.
        def add(item)
          id = items.insert(id: item.id, name: item.name, price: item.price)
          item.id = id

          item
        end

        # Deletes the item from the repository.
        def delete(item)
          items.where(id: item.id).delete
        end

        private

        def items
          @db[:items]
        end
      end
    end
  end
end
