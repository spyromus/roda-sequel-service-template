# frozen_string_literal: true

module RestApi
  # App prodivdes routing
  class App
    hash_branch '/api', 'health' do |r|
      r.get true do
        'OK'
      end
    end
  end
end
