# frozen_string_literal: true

module RestApi
  # App prodivdes routing
  class App
    require_relative 'v1/items'

    hash_branch '/api', 'v1', &:hash_branches
  end
end
