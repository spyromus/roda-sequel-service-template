# frozen_string_literal: true

module RestApi
  # App prodivdes routing
  class App
    def create_item(trx, params, id: nil)
      # Create new record
      item = Item.new(
        id: id,
        name: params['name'],
        price: params['price']
      )

      trx.items.add(item)
    end

    hash_branch '/api/v1', 'items' do |r|
      # Lists items
      r.get true do
        { items: resolve(:items_view).all }
      end

      # Returns the item details
      r.get Integer do |id|
        resolve(:item_view).(id)
      end

      # Creates item
      r.post true do
        results = message_bus.handle(CreateItemCommand.new(name: r.params['name'], price: r.params['price']))
        item = results.first

        response.status = 201
        item.to_h
      end

      # Updates or creates an item
      r.is Integer, method: :put do |id|
        results = message_bus.handle(UpdateItemCommand.new(id: id, name: r.params['name'], price: r.params['price']))
        item = results.first

        item.to_h
      end

      # Deletes the item
      r.is Integer, method: :delete do |id|
        message_bus.handle(DeleteItemCommand.new(id: id))

        r.halt 204
      end
    end
  end
end
