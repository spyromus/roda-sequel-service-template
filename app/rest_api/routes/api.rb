# frozen_string_literal: true

module RestApi
  # App prodivdes routing
  class App
    require_relative 'api/health'
    require_relative 'api/v1'

    hash_branch 'api', &:hash_branches
  end
end
