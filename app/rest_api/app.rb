# frozen_string_literal: true

require 'roda'

module RestApi
  # App provides routing
  class App < Roda
    plugin :hash_branches
    plugin :json
    plugin :halt

    Dir.glob('./routes/**/*.rb', base: __dir__).each do |route_file|
      require_relative route_file
    end

    route(&:hash_branches)

    private

    def resolve(dep_name)
      ::Container[dep_name]
    end

    def message_bus
      resolve(:message_bus)
    end
  end
end
