# frozen_string_literal: true

module ServiceLayer
  module Handlers
    class CreateItem
      def initialize(uow:)
        @uow = uow
      end

      def call(cmd)
        item = create_item(cmd)

        HandlerResponse.new(
          events: [ItemCreatedEvent.from_item(item)],
          results: [item]
        )
      end

      private

      def create_item(cmd)
        @uow.() do |t|
          item = Item.new(name: cmd.name, price: cmd.price)
          t.items.add(item)
          t.commit

          item
        end
      end
    end
  end
end
