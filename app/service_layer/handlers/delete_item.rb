# frozen_string_literal: true

module ServiceLayer
  module Handlers
    class DeleteItem
      def initialize(uow:)
        @uow = uow
      end

      def call(cmd)
        deleted = delete_item(cmd)

        HandlerResponse.new(
          events: deleted ? [ItemDeletedEvent.new(id: cmd.item_id)] : [],
          results: [deleted]
        )
      end

      private

      def delete_item(cmd)
        @uow.() do |t|
          item = t.items.find(cmd.item_id)
          next false unless item

          t.items.delete(item)
          t.commit

          true
        end
      end
    end
  end
end
