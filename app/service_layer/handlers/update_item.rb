# frozen_string_literal: true

module ServiceLayer
  module Handlers
    class UpdateItem
      def initialize(uow:)
        @uow = uow
      end

      def call(cmd)
        item = update_item(cmd)

        HandlerResponse.new(
          events: [ItemUpdatedEvent.from_item(item)],
          results: [item]
        )
      end

      private

      def update_item(cmd)
        @uow.() do |t|
          # Replace existing record
          item = t.items.find(cmd.id)
          t.items.delete(item) if item

          item = Item.new(id: cmd.id, name: cmd.name, price: cmd.price)
          t.items.add(item)
          t.commit

          item
        end
      end
    end
  end
end
