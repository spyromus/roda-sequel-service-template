# frozen_string_literal: true

module ServiceLayer
  module Handlers
    class Publish
      def initialize(logger:)
        @logger = logger
      end

      def call(msg)
        @logger.info "publishing: event=#{msg.event_name} version=#{msg.event_version} #{msg.payload}"
      end
    end
  end
end
