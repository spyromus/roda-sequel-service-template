# frozen_string_literal: true

require 'logger'
require 'retryable'

module ServiceLayer
  # MessageBus provides the pipeline for handling messages.
  class MessageBus
    def initialize(logger:, mapping:, retries: 5)
      @logger = logger
      @mapping = mapping
      @retries = retries
    end

    def handle(message)
      queue = [message]

      queue.each_with_object([]) do |msg, results|
        resp = handle_message(msg)

        results.concat(resp.results)
        queue.concat(resp.events)
      end
    end

    private

    def handle_message(msg)
      return handle_event(msg) if msg.respond_to?(:event?) && msg.event?
      return handle_command(msg) if msg.respond_to?(:command?) && msg.command?

      raise ArgumentError, "unknown type of message: #{msg.class.name}"
    end

    def handle_event(event)
      handlers = handlers(event)
      response = HandlerResponse.new
      return response unless handlers

      Array[handlers].each do |handler|
        Retryable.retryable(tries: @retries, sleep: ->(n) { 2**n }) do
          response.append_response(handle_event_with_handler(handler, event))
        end
      end

      response
    end

    # Returns events to add to the queue and handle next
    def handle_event_with_handler(handler, event)
      wrap_response(handler.(event))
    rescue StaleObject
      retry
    end

    def handle_command(command)
      handler = handlers(command)
      return HandlerResponse.new unless handler

      handle_command_with_handler(handler, command)
    end

    # Returns HandlerResponse
    def handle_command_with_handler(handler, command)
      wrap_response(handler.(command))
    rescue StaleObject
      retry
    end

    # Handlers for the message
    def handlers(msg)
      class_name = msg.class.name
      handlers = @mapping[class_name]

      @logger.info "unspecified handler for: #{class_name}" unless handlers

      handlers
    end

    def wrap_response(resp)
      return resp if resp.is_a? HandlerResponse

      HandlerResponse.new(results: [resp])
    end
  end
end
