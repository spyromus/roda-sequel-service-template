# frozen_string_literal: true

module ServiceLayer
  # HandlerResponse encapsulates new events and results from a handler
  class HandlerResponse
    attr_reader :events, :results

    def initialize(events: nil, results: nil)
      @events = events || []
      @results = results || []
    end

    def append_response(response)
      events.concat(response.events)
      results.concat(response.results)
    end
  end
end
