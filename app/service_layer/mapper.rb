# frozen_string_literal: true

module ServiceLayer
  # Mapper provides handlers for messages.
  class Mapper
    def initialize(uow:, logger:)
      @uow = uow
      @logger = logger
    end

    def [](name)
      case name
      when 'CreateItemCommand' then Handlers::CreateItem.new(uow: @uow)
      when 'DeleteItemCommand' then Handlers::DeleteItem.new(uow: @uow)
      when 'UpdateItemCommand' then Handlers::UpdateItem.new(uow: @uow)

      when 'ItemCreatedEvent', 'ItemDeletedEvent', 'ItemUpdatedEvent'
        Handlers::Publish.new(logger: @logger)
      end
    end
  end
end
