# frozen_string_literal: true

# Item is a sample domain item.
class Item
  attr_accessor :id, :name, :price

  def initialize(name:, price:, id: nil)
    @id = id
    @name = name
    @price = price
  end

  def to_h
    { id: id, name: name, price: price }
  end
end
