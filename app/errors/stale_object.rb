# frozen_string_literal: true

# StaleObject error indicates there was an attempt to update
# an entity while its state has already changed.
#
# This error is raised by repositories when optimistic locking is used.
class StaleObject < StandardError
end
