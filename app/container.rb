# frozen_string_literal: true

require 'dry-container'
require 'logger'

# Container is dependency injection container
class Container
  extend Dry::Container::Mixin

  # General
  register(:config, Config.new)
  register(:logger, Logger.new($stdout))
  register(:db, Adapters::Sequel::DatabaseFactory.new(resolve(:config).database_url).create_connection)

  # Messaging
  register(:uow) { Adapters::Sequel::UnitOfWork.new(db: resolve(:db)) }
  register(:mapping) { ServiceLayer::Mapper.new(uow: resolve(:uow), logger: resolve(:logger)) }
  register(:message_bus) { ServiceLayer::MessageBus.new(logger: resolve(:logger), mapping: resolve(:mapping)) }

  # Views
  register(:item_view) { Adapters::Sequel::Views::ItemView.new(db: resolve(:db)) }
  register(:items_view) { Adapters::Sequel::Views::ItemsView.new(db: resolve(:db)) }
end
