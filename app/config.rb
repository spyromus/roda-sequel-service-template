# frozen_string_literal: true

# Config provides application configuration.
class Config
  def database_url
    ENV.fetch('DATABASE_URL')
  end
end
