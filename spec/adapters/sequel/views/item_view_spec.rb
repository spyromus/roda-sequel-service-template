# frozen_string_literal: true

RSpec.describe Adapters::Sequel::Views::ItemView do
  subject(:view) { described_class.new(db: DB) }

  it 'returns nil when no records' do
    expect(view.(0)).to be_nil
  end

  it 'returns the item data' do
    item_id = DB[:items].insert(name: 'name', price: 123)

    expect(view.(item_id)).to eq(
      id: item_id,
      name: 'name',
      price: 123
    )
  end
end
