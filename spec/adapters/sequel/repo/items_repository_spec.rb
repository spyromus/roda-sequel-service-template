# frozen_string_literal: true

RSpec.describe Adapters::Sequel::Repo::ItemsRepository do
  subject(:repo) { described_class.new(db: DB) }

  describe '#find' do
    it 'returns existing record' do
      item_id = DB[:items].insert(name: 'name', price: 1)

      item = repo.find(item_id)
      expect(item).to be_a Item
      expect(item.id).to eq item_id
      expect(item.name).to eq 'name'
      expect(item.price).to eq 1
    end

    it 'returns nil for missing record' do
      expect(repo.find(0)).to be_nil
    end
  end

  describe '#add' do
    it 'adds the item' do
      item = Item.new(name: 'name', price: 1)

      res = repo.add(item)
      expect(res).to be_a Item
      expect(item.id).not_to be_nil
      expect(item.name).to eq item.name
      expect(item.price).to eq item.price
    end
  end

  describe '#delete' do
    it 'deletes existing item' do
      item = Item.new(name: 'name', price: 1)
      item = repo.add(item)

      repo.delete(item)

      expect(DB[:items].first(id: item.id)).to be_nil
    end

    it 'does not raise an error when item does not exist' do
      item = Item.new(id: 0, name: 'name', price: 1)
      expect { repo.delete(item) }.not_to raise_error
    end
  end
end
