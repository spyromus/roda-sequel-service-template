# frozen_string_literal: true

require 'logger'

RSpec.describe ServiceLayer::MessageBus do
  subject(:handle) { bus.handle(msg) }

  let(:bus) do
    described_class.new(
      logger: logger,
      mapping: mapping,
      retries: retries
    )
  end

  let(:fake_event_cls) do
    Class.new do
      def event?
        true
      end
    end
  end

  let(:fake_event_two_cls) do
    Class.new do
      def event?
        true
      end
    end
  end

  let(:fake_command_cls) do
    Class.new do
      def command?
        true
      end
    end
  end

  let(:logger) { instance_double(Logger, info: nil) }
  let(:mapping) { {} }
  let(:retries) { 2 }

  before do
    stub_const('FakeEvent', fake_event_cls)
    stub_const('FakeEventTwo', fake_event_two_cls)
    stub_const('FakeCommand', fake_command_cls)
  end

  context 'when msg is neither command nor event' do
    let(:msg) { :unknown }

    it 'raises error' do
      expect { handle }.to raise_error ArgumentError, 'unknown type of message: Symbol'
    end
  end

  context 'when event handler is unknown' do
    let(:msg) { fake_event_cls.new }

    it 'logs the error' do
      handle
      expect(logger).to have_received(:info).with('unspecified handler for: FakeEvent')
    end
  end

  context 'when command handler is unknown' do
    let(:msg) { fake_command_cls.new }

    it 'logs the error' do
      handle
      expect(logger).to have_received(:info).with('unspecified handler for: FakeCommand')
    end
  end

  context 'when handling known event' do
    let(:msg) { FakeEvent.new }
    let(:mapping) { { 'FakeEvent' => [handler1, handler2] } }
    let(:handler1) { instance_double(Proc, call: 1) }
    let(:handler2) { instance_double(Proc, call: 2) }

    it 'invokes both handlers and collects the results' do
      expect(handle).to eq [1, 2]
    end
  end

  context 'when handling event produces new events' do
    let(:msg) { FakeEvent.new }
    let(:mapping) do
      {
        'FakeEvent' => [handler1],
        'FakeEventTwo' => [handler2]
      }
    end

    let(:handler1) { instance_double(Proc, call: ServiceLayer::HandlerResponse.new(events: [FakeEventTwo.new], results: [1])) }
    let(:handler2) { instance_double(Proc, call: 2) }

    it 'invokes both handlers and collects the results' do
      expect(handle).to eq [1, 2]
    end
  end

  context 'when handling event fails' do
    let(:msg) { FakeEvent.new }
    let(:mapping) { { 'FakeEvent' => [handler] } }
    let(:handler) { instance_double(Proc, call: nil) }

    before do
      # Configure handler to raise an error and then return the value
      call_count = 0
      allow(handler).to receive(:call) do
        call_count += 1
        raise StandardError if call_count == 1

        :result
      end
    end

    it 'retries' do
      expect(handle).to eq [:result]
    end
  end

  context 'when handling a known command' do
    let(:msg) { FakeCommand.new }
    let(:mapping) { { 'FakeCommand' => handler } }
    let(:handler) { instance_double(Proc, call: 1) }

    it 'invokes handler and collects the result' do
      expect(handle).to eq [1]
    end
  end
end
