FROM ruby:3.0.5-alpine

# This is for nio4r (puma dependency)

WORKDIR /app

RUN bundle config set --local without "development test"

COPY Gemfile .
RUN apk update \
  && apk add --no-cache build-base \
  && bundle install \
  && apk del build-base

COPY . .

EXPOSE 3000
ENTRYPOINT [ "./entrypoint.sh" ]