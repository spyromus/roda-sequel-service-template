IMAGE_NAME=act

include .env

.PHONY: run
run:
	rerun rackup -- -p 3000

.PHONY: guard
guard:
	bundle exec guard

.PHONY: migrate
migrate:
	bundle exec sequel -m migrations ${DATABASE_URL}

.PHONY: migrate-to
migrate-to:
	bundle exec sequel -m migrations -M ${VERSION} ${DATABASE_URL}

.PHONY: build
build:
	docker build -t ${IMAGE_NAME} --platform=amd64 .
