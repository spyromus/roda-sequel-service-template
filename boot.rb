# frozen_string_literal: true

require 'zeitwerk'

loader = Zeitwerk::Loader.for_gem
loader.push_dir("#{__dir__}/app")
loader.push_dir("#{__dir__}/app/errors")
loader.push_dir("#{__dir__}/app/messages")
loader.push_dir("#{__dir__}/app/models")
loader.ignore("#{__dir__}/migrations")
loader.ignore("#{__dir__}/spec")
loader.setup
