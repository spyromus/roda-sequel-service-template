# frozen_string_literal: true

Sequel.migration do
  change do
    create_table(:items) do
      primary_key :id
      String :name
      Integer :price
    end
  end
end
