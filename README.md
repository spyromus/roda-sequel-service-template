# Service template

Template project for Ruby microservices with minimal requirements.

## Features

- [x] DDD layout with clear domain / adapters separation
- [x] Dependency injection
- [x] Database migrations
- [x] Docker container
- [x] Message bus, events, commands, handlers
- [-] RSpec tests
- [ ] Swagger doc

## Technologies

- [x] Roda
- [x] Sequel

## Migrations

Sequal migrations are used as described in the [official doc](http://sequel.jeremyevans.net/rdoc/files/doc/migration_rdoc.html).

Migration files sit in `migrations` and are named as:

```
YYYYMMDDHHMMSS_brief_description.rb
```

Migrate up:

```shell
$ make migrate
```

Migrate to a specific version (0 - rollback everything):

```shell
$ make migrate-to VERSION=<...>
```

## Running

```shell
$ make migrate
$ make run
```

## Building releases

The following command builds the Docker image with the name in Makefile `IMAGE_NAME`.

```shell
$ make build
```

Test running:

```shell
$ docker run --rm -it -p 3000:3000 -e DATABASE_URL=sqlite://app.db <image-name>
```

Then:

```shell
$ curl 0:3000/api/health
OK
```
