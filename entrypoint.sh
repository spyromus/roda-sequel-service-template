#!/bin/sh

# Migrate database
bundle exec sequel -m migrations $DATABASE_URL

# Launch app
bundle exec rackup -o 0.0.0.0 -p 3000
