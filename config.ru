# frozen_string_literal: true

require 'dotenv/load'
require_relative 'boot'

run RestApi::App.freeze.app
